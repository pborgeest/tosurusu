
package main

import (
	"os"
	"io"
	"fmt"
	"flag"
	"bufio"
	"strings"
	"unicode/utf8"
)

type Line string

var isShowingInitials bool = false
func processFlags() {
	flag.BoolVar(&isShowingInitials, "i", false, "Prompt with the initials of the answer line")
	flag.Parse()
}

// from http://stackoverflow.com/questions/6141604/go-readline-string
func ReadStrings(r *bufio.Reader) []Line {
	output := make([]Line,1)

    line, err := r.ReadString('\n')
    for err == nil {
        output = append(output,Line(strings.TrimSpace(line)))
        line, err = r.ReadString('\n')
    }
    if err != io.EOF {
		fmt.Printf("ERROR: %v",err)
    }
	return output
}

func (ln Line) trim() Line {
	return ln[:len(ln)-1]
}

func (ln Line) greyer() Line {
	return "<font color=\"gray\">" + ln + "</font>"
}

// explode splits s into an array of UTF-8 sequences, one per Unicode character (still strings) up to a maximum of n (n < 0 means no limit).
// Invalid UTF-8 sequences become correct encodings of U+FFF8.
// From golang.org/src/pkg/strings/strings.go
func explode(s string, n int) []string {
	if n == 0 {
		return nil
	}
	l := utf8.RuneCountInString(s)
	if n <= 0 || n > l {
		n = l
	}
	a := make([]string, n)
	var size int
	var ch rune
	i, cur := 0, 0
	for ; i+1 < n; i++ {
		ch, size = utf8.DecodeRuneInString(s[cur:])
		a[i] = string(ch)
		cur += size
	}
	// add the rest, if there is any
	if cur < len(s) {
		a[i] = s[cur:]
	}
	return a
}

func getInitialOfWord(s string) string {
	exploded := explode(s,2)
	return exploded[0]
}
	

func (ln Line) initials() Line {
	var line string = string(ln)
	inits := make([]string,0)
	for _, word := range strings.Split(line," ") {
		inits = append(inits,getInitialOfWord(word))
	}
	return Line(strings.Join(inits," "))
}

func main() {
	processFlags()
	r := bufio.NewReader(os.Stdin)
	lines := ReadStrings(r) 
	// i is the index of the current answer line: the two previous lines are used as prompts
	for i := 2; i < len(lines)-1; i++ {
		fmt.Print(lines[i-2],"<br>")
		fmt.Print(lines[i-1],"<br>")
		fmt.Print(lines[i])
		if isShowingInitials {
			fmt.Print("<br>",lines[i+1].initials().greyer())
		}
		fmt.Print("\t")
		fmt.Print(lines[i+1],"<br>")
		if i<(len(lines)-2) {
			fmt.Print(lines[i+2].greyer())
		}
		fmt.Print("\n")
	}
}
